﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using WpfApplDemo2018.Model;
using WpfApplDemo2018.ViewModel;

namespace WpfAppIDemo2018.View
{
    /// <summary>
    /// Логика взаимодействия для WindowRole.xaml
    /// </summary>
    public partial class WindowRole : Window
    {
        public WindowRole()
        {
            InitializeComponent();

            DataContext = new RoleViewModel();
                
        }
        private void listview1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }

}
